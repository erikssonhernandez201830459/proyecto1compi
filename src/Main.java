import Abstract.Instruction;
import entorno.*;
import grammar.GrammarLexer;
import grammar.GrammarParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;
import java.util.*;
public class Main {
    public static void main(String[] args) {

        CharStream charStream = CharStreams.fromString("""
                int var1 = 10 + 10;
                int var2 = 15;
                print(var1);
                print("hola");
                print(2+2);
                {
                    int var1 = 4;
                    print(var1);
                    {
                        int var1 = 123+ var2;
                        print(var1);
                    }
                    print(var1);
                }
                 
                print(var1+ var2);
                """);

        GrammarLexer grammarLexer = new GrammarLexer(charStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(grammarLexer);

        GrammarParser grammarParser = new GrammarParser(commonTokenStream);
        GrammarParser.StartContext startContext = grammarParser.start();

        Visitor visitor = new Visitor();
        SymbolTable globalTable = new SymbolTable(null);

        Object treeAst = visitor.visit(startContext);

        for (Object item: (List<?>) treeAst) {
            ((Instruction) item).interpret(globalTable);
        }

        List<String> ruleNames = Arrays.asList(grammarParser.getRuleNames());
        TreeViewer CST = new TreeViewer(ruleNames, startContext);
        CST.open();

        List<String>  rulesName = Arrays.asList(grammarParser.getTokenNames());
        TreeViewer AST = new TreeViewer(rulesName,startContext);
        AST.open();

        Console console = Console.getInstance();
        System.out.println(console.console);
    }
}