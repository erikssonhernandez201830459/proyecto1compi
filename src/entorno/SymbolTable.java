package entorno;

import java.util.HashMap;

public class SymbolTable {
    private HashMap<String, Symbol> SymbolTable;
    private SymbolTable prev;

    public SymbolTable(SymbolTable prev) {
        this.prev = prev;
        SymbolTable = new HashMap<String, Symbol>();
    }

    public boolean addSymbol(String name, Symbol newSymbol) {

        if (SymbolTable.containsKey(name.toLowerCase())) {
            //Add list error
            System.out.println("This variable already exist in current context");
            return false;
        }

        SymbolTable.put(name.toLowerCase(), newSymbol);

        return true;
    }

    public Symbol getSymbol(String name) {

        SymbolTable currentSymbolTable = this;

        while (currentSymbolTable != null) {
            if (currentSymbolTable.getSymbolTable().containsKey(name)) {
                return currentSymbolTable.getSymbolTable().get(name);
            }

            currentSymbolTable = currentSymbolTable.getPrev();
        }
        return null;
    }

    public HashMap<String, Symbol> getSymbolTable() {
        return SymbolTable;
    }

    public void setSymbolTable(HashMap<String, Symbol> symbolTable) {
        SymbolTable = symbolTable;
    }

    public SymbolTable getPrev() {
        return prev;
    }

    public void setPrev(SymbolTable prev) {
        this.prev = prev;
    }
}
