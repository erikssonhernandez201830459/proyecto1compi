package Abstract;

import entorno.ReturnType;
import entorno.SymbolTable;

public abstract class Instruction {
    public int row;
    public int column;

    public Instruction(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public abstract ReturnType getValue(SymbolTable table);
    public abstract Object interpret(SymbolTable table);
}
