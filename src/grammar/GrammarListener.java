// Generated from /home/erikssonherlo/Escritorio/Compi 2/Proyecto1/Grammar.g4 by ANTLR 4.10.1
package grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(GrammarParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(GrammarParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#listaInstrucciones}.
	 * @param ctx the parse tree
	 */
	void enterListaInstrucciones(GrammarParser.ListaInstruccionesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#listaInstrucciones}.
	 * @param ctx the parse tree
	 */
	void exitListaInstrucciones(GrammarParser.ListaInstruccionesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blck}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void enterBlck(GrammarParser.BlckContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blck}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void exitBlck(GrammarParser.BlckContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declar}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void enterDeclar(GrammarParser.DeclarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declar}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void exitDeclar(GrammarParser.DeclarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ptr}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void enterPtr(GrammarParser.PtrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ptr}
	 * labeled alternative in {@link GrammarParser#instruction}.
	 * @param ctx the parse tree
	 */
	void exitPtr(GrammarParser.PtrContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(GrammarParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(GrammarParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(GrammarParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(GrammarParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(GrammarParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(GrammarParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(GrammarParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(GrammarParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterStrExpr(GrammarParser.StrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitStrExpr(GrammarParser.StrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterOpExpr(GrammarParser.OpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitOpExpr(GrammarParser.OpExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterAtomExpr(GrammarParser.AtomExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code atomExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitAtomExpr(GrammarParser.AtomExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(GrammarParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(GrammarParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(GrammarParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GrammarParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(GrammarParser.IdExprContext ctx);
}