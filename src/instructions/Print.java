package instructions;

import Abstract.Instruction;
import entorno.Console;
import entorno.ReturnType;
import entorno.SymbolTable;

public class Print extends Instruction {
    public Object expression;

    public Print(int row, int column, Object expression) {
        super(row, column);
        this.expression = expression;
    }

    @Override
    public ReturnType getValue(SymbolTable table) {
        return null;
    }

    @Override
    public Object interpret(SymbolTable table) {

        ReturnType expr = ((Instruction) this.expression).getValue(table);

        if (expr.value == null) {
            //Semantic error
            return null;
        }

        Console console = Console.getInstance();

        console.console += expr.value.toString() + "\n";

        return null;
    }
}
