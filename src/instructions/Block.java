
package instructions;
import Abstract.Instruction;
import entorno.*;
import entorno.ReturnType;
import entorno.SymbolTable;

import java.util.List;

public class Block extends Instruction {

    public Object listInstructions;

    public Block(int row, int column, Object listInstructions) {
        super(row, column);
        this.listInstructions = listInstructions;
    }

    @Override
    public ReturnType getValue(SymbolTable table) {
        return null;
    }

    @Override
    public Object interpret(SymbolTable table) {

        SymbolTable newSymbolTable = new SymbolTable(table);

        for (Object item: (List<?>) listInstructions){
            ((Instruction) item).interpret(newSymbolTable);
        }

        return null;
    }

}
